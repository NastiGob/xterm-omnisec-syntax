# xterm-omnisec-syntax theme

It's a dark theme for Atom.

![A screenshot of your theme](https://gitlab.com/NastiGob/xterm-omnisec-syntax/raw/master/img/exemple_syntaxe.png)
